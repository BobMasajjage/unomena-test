# manage.py


import os
import datetime

from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand


from unomena_test import app, db
from unomena_test.models import User

app.config.from_object(os.environ['APP_SETTINGS'])

migrate = Migrate(app, db)
manager = Manager(app)

# migrations
manager.add_command('db', MigrateCommand)

@manager.command
def create_db():
    """Creates the db tables."""
    db.create_all()


@manager.command
def drop_db():
    """Drops the db tables."""
    db.drop_all()


@manager.command
def create_admin():
    """Creates the admin user."""
    db.session.add(User(
        first_name = "cuper",
        last_name = "user",
        email="ad@mail.com",
        password="amin",
        admin=True,
        confirmed=True,
        confirmed_on=datetime.datetime.now())
    )
    db.session.commit()


if __name__ == '__main__':
    manager.run()
