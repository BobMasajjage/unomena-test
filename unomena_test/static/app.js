'use static'
/*
 * define the unomeman test module here
 * adding the ngRout as a dependence
*/

var testAPP = angular.module('unomenaTest', ['ngRoute']); 

testAPP.config(['$routeProvider','$locationProvider', 
    function($routeProvider, $locationProvider){
        $routeProvider
        .when("/",{
            templateUrl:"templates/main/index.html",
            controller:"TodoController",
            controllerAs:"TodoCTRL"
        })
        .otherwise({
            redirectTo:"/login"
        })
    }]);

testAPP.controller('TodoController', ['$scope', function($scope){

		console.log("angular Loadded and working");
}]);