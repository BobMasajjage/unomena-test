# todoApi/models.py

import datetime

from unomena_test import db


class Todo(db.Model):

	__tablename__ = "todos"

	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	description =  db.Column(db.String,  nullable=False)
	due_date = db.Column(db.DateTime, nullable=True)


	def __init__(self, description, due_date):

		self.description = description
		self.due_date = due_date





