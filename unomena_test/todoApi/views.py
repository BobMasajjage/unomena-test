
# /todoApi/views.py

from .models import Todo
from unomena_test import app
from flask.ext.login import login_required, current_user
from flask_restful import reqparse, abort, Resource, Api

# define the app api here
api = Api(app)


class TodoList(Resource):

	@login_required
	def get(self):
		if not current_user.is_authenticated:
			return abort(401, message = "You must be logged in")
		currentUser = current_user
		# todos=Todo.query.filter_by(user_id = current_user.id).order_by(Todo.due_date.desc()).all()
		todos=Todo.query.filter_by(user_id = current_user.id).all()
		return todos


    @login_required
	def post(self):
		if not request.method == "POST":
			return abort(400, message = "BAD REQUEST")
		if not current_user.is_authenticated:
			return abort(401, message="You are must be logged in")

		currentUser = current_user
		currentUserId = currentUser.id
		todo_desc = request.data.get("description")
		due_time = request.data.get("due")
		new_todo = Todo(todo_desc, due_time)
        new_todo.user_id = currentUserId
        db.session.add(new_todo)
        db.session.commit(new_todo)


api.add_resource(TodoList, '/todos')
