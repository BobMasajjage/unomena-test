
# unomena_test/util.py

from flask.ext.testing import TestCase

from unomena_test import app, db
from unomena_test.models import User


class BaseTestCase(TestCase):

    def create_app(self):
        app.config.from_object('unomena_test.config.TestingConfig')
        return app

    @classmethod
    def setUpClass(self):
        db.create_all()
        user = User(
            first_name = 'super',
            last_name = 'user',
            email="test@user.com",
            password="just_a_test_user",
            confirmed=False
        )
        db.session.add(user)
        db.session.commit()

    @classmethod
    def tearDownClass(self):
        db.session.remove()
        db.drop_all()
